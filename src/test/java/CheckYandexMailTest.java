import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.HomePage;

import java.util.HashMap;

import static com.codeborne.selenide.Selenide.open;

public class CheckYandexMailTest extends BaseTest {

    private final Logger logger = LogManager.getLogger(CheckYandexMailTest.class);

    @DataProvider(name = "dp")
    public Object[] testData() {
        return getTestData();
    }

    @Test(dataProvider = "dp")
    public void yandexMailTest(HashMap<String, String> testData) {
        homePage = open("https://mail.yandex.ru", HomePage.class);
        logger.info("Check Yandex.Mail service — start");
        logger.info("Locate to Yandex.Mail homepage");
        loginPage = homePage.in();
        logger.info("Enter login");
        loginPage.setUserName(testData.get("Login"));
        logger.info("Enter password");
        MailboxPage = loginPage.setPassword(testData.get("Password"));
        logger.info("Enter Mailbox Page");
        newMailPage = MailboxPage.clickComposeButton();
        logger.info("Compose a letter");
        logger.info("Fill the 'addressee' field in");
        newMailPage.setAddressee(testData.get("Addressee"));
        logger.info("Fill the 'subject' field in");
        newMailPage.setMessageSubject(testData.get("EmailSubject"));
        logger.info("Send an email");
        newMailPage.sendEMail();
        logger.info("Return to inbox");
        MailboxPage = newMailPage.returnToInbox();
        logger.info("Check a message to delete");
        MailboxPage.checkMail();
        logger.info("Delete the email");
        MailboxPage.deleteMail();
        logger.info("Open a trash box");
        MailboxPage.openTrash();
        logger.info("Check a message to move and move in to inbox");
        MailboxPage.moveMailToInbox();
        logger.info("Search for a particular addresser");
        MailboxPage.search(testData.get("Addressee"));
        logger.info("Mark an email as favourite");
        MailboxPage.markEMailAsFavorite();
        logger.info("End test");
    }

}
