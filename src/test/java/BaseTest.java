import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.Selenide;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import pages.HomePage;
import pages.LoginPage;
import pages.MailboxPage;
import pages.NewMailPage;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;

public class BaseTest {

/* -------------Страницы-------------*/
    /*Главная страница*/
    static HomePage homePage;
    /*Страница авторизации*/
    static LoginPage loginPage;
    /*Страница почтового ящика*/
    static MailboxPage MailboxPage;
    /*Страница создания нового письма*/
    static NewMailPage newMailPage;

/*-----------Получение тестовых данных из эксель-таблицы---------*/
        public Object[] getTestData() {
            HashMap<String, String> data = new HashMap<>();
            try (FileInputStream file = new FileInputStream("/home/neksword/Документы/testdata.xlsx")) {
                XSSFWorkbook workbook = new XSSFWorkbook(file);
                XSSFSheet sheet = workbook.getSheetAt(0);
                Iterator<Row> rows = sheet.iterator();
                Row row;
                while (rows.hasNext()) {
                    row = rows.next();
                    for (Cell cell : row) {
                        if (cell.getColumnIndex() == 0) {
                            continue;
                        }
                        String actualCell = cell.getStringCellValue();
                        if (actualCell == null) {
                            continue;
                        }
                        String firstCellOfTheRow = row.getCell(0)
                                                      .getStringCellValue();
                        data.put(firstCellOfTheRow,
                                 actualCell);
                    }
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
            Object[] testData = new Object[1];
            testData[0] = data;
            return testData;
        }
    /*Конфигурация драйвера и вход на домашнюю страницы Яндекс.Почты*/
    @BeforeSuite
    public static void beforeTest() {
        System.setProperty("webdriver.chromedriver.driver", "/home/neksword/chromedriver/chromedriver");
        Configuration.browser = "chrome";
        Configuration.remote = "http://" + "127.0.0.1" + ":" + "4444" + "/wd/hub";
        Configuration.reportsFolder = "build/reports/tests";
        Configuration.browserSize = "1920x1080";
        Configuration.timeout = 10000;
    }
    /*Закрытие драйвера*/
    @AfterSuite
    public static void close() {
        Selenide.close();
    }
}
