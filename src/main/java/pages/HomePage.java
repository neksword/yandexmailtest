package pages;

import com.codeborne.selenide.Selenide;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selectors.byLinkText;
/*Класс, описывающий главную страницу почты*/
public class HomePage {

    /*Нажатие кнопки войти*/
    @Step("Вход на главную страницу Яндекс.Почты")
    public LoginPage in() {
        String inButton = "Войти";
        Selenide.$(byLinkText(inButton)).click();
        return Selenide.page(LoginPage.class);
    }
}
