package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selectors.*;

/*Класс, описывающий страницу почтового ящика*/

public class MailboxPage {

    /*Ссылка на папку "Удалённые"*/
    private String trashLink = "//span[text()='Удалённые']/parent::a";

    /*Выбор письма; чек-бокс*/
    private String inputs = "_nb-checkbox-flag";

    /*Нажатие кнопки "Написать"*/
    @Step("Нажимаем кнопку 'Написать'")
    public NewMailPage clickComposeButton() {
        String composeButton = "mail-ComposeButton-Text";
        Selenide.$(byClassName(composeButton))
                .click();
        return Selenide.page(NewMailPage.class);
    }

    /*Отметка письма*/
    @Step("Отмечаем письмо")
    public void checkMail() {
        Selenide.$$(byClassName(inputs))
                .get(0)
                .click();
    }

    /*Удаление письма*/
    @Step("Удаляем письмо")
    public void deleteMail() {
        String deleteButton = "mail-Toolbar-Item_delete";
        Selenide.$(byClassName(deleteButton))
                .click();
    }

    /*Открытие корзины*/
    @Step("Открываем корзину")
    public void openTrash() {
        Selenide.$x(trashLink)
                .click();
        Selenide.$(byText("Письма по месяцам:"))
                .shouldBe(Condition.visible);
    }

    /*Перемещение письма во "Входящие"*/
    @Step("Перемещаем удаленное письмо во 'Входящие'")
    public void moveMailToInbox() {
        /*Ссылка на "Входящие" из меню "В папку"*/
        String moveToInboxLink = "//a[@title = 'Входящие']";
        /*Ссылка на "Входящие" из левого контекстного меню*/
        String inboxLink = "//span[text()='Входящие']/parent::a";
        String enabledTrashLink = "mail-NestedList-Item_current";
        /*Ожидание перехода в папку "Удалённые". Письмо будет перемещено оттуда*/
        Selenide.$x(trashLink).shouldHave(Condition.cssClass(enabledTrashLink));
        Selenide.$(byClassName(inputs)).shouldBe(Condition.visible).click();
        /*Перемещение письма во "Входящие"*/
        Selenide.$(by("title", "В папку (m)")).shouldBe(Condition.visible).click();
        Selenide.$x(moveToInboxLink).click();
        Selenide.$x(inboxLink).click();
        Selenide.$(byText("Письма по месяцам:")).shouldBe(Condition.visible);
    }

    /*Поиск письма*/
    @Step("Ищем письмо по адресу")
    public void search(String addressee) {
        String searchInputField = "//div[@class = 'mail-IconsBox']/following-sibling::input";
        /*Ожидание загрузки результатов поиска*/
        String searchTitle = "mail-MessagesSearchInfo-Title";
        Selenide.$x(searchInputField)
                .append(addressee)
                .pressEnter();
        Selenide.$(byClassName(searchTitle)).shouldBe(Condition.visible);
    }

    /*Отметка письма как "Важное"*/
    @Step("Отмечаем письмо как 'Важное'")
    public void markEMailAsFavorite() {
        String markAsFavoriteLink = "//a[text() = 'Важные']";
        Selenide.$$(byClassName(inputs))
                .get(0)
                .click();
        Selenide.$(by("title", "Метка (l)")).click();
        Selenide.$x(markAsFavoriteLink).click();
    }
}

