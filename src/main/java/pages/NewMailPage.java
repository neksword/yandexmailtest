package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import ru.yandex.qatools.allure.annotations.Step;

import static com.codeborne.selenide.Selectors.byName;
import static com.codeborne.selenide.Selectors.byPartialLinkText;

/*Класс, описывающий страницу нового письма*/

public class NewMailPage {


    @Step("Вводим адресата")
    public void setAddressee(String addressee) {
        String sendTo = "to";
        Selenide.$(byName(sendTo)).shouldBe(Condition.visible).sendKeys(addressee);
    }


    @Step("Вводим тему письма")
    public void setMessageSubject(String mailSubject) {
        String messageSubject = "subj-dbe590784f66305a6db0af72da188401785a0c75";
        Selenide.$("input[name='" + messageSubject + "']").sendKeys(mailSubject);
        }

    /*Нажатие кнопки "Отправить"*/
    @Step("Отправляем письмо")
    public void sendEMail() {
        String sendMail = "//span[text()='Отправить']";
        Selenide.$x(sendMail).click();
    }

    /*Возврат в папку "Входящие"*/
    @Step("Возвращаемся во 'Входящие'")
    public MailboxPage returnToInbox() {
        String returnToInbox = "Перейти во";
        Selenide.$(byPartialLinkText(returnToInbox)).click();
        return Selenide.page(MailboxPage.class);
    }
}
