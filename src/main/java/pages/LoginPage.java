package pages;

import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;
import ru.yandex.qatools.allure.annotations.Step;

/*Класс, описывающий страницу входа */
public class LoginPage {
    /*Кнопка "Войти"*/
    private String inButton = "button";

    /*Ввод логина
    strUserName - логин для входа
     */
    @Step("Вводим логин")
    public void setUserName(String strUserName) {
        String userName = "login";
        Selenide.$("input[name='" + userName + "']").sendKeys(strUserName);
        Selenide.$(By.tagName(inButton)).click();
    }
    /*Ввод пароля
    strPassword - пароль для входа
     */
    @Step("Вводим пароль")
    public MailboxPage setPassword(String strPassword) {
        String password = "passwd";
        Selenide.$("input[name='" + password + "']").sendKeys(strPassword);
        Selenide.$(By.tagName(inButton)).click();
        return Selenide.page(MailboxPage.class);
    }

}
